<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSallaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sallary', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('employes_id');
            $table->foreign('employes_id')
                        ->references('id')
                        ->on('employes')
                        ->onDelete('cascade');
            $table->bigInteger('sallary_definition_id');
            $table->foreign('sallary_definition_id')
                        ->references('id')
                        ->on('sallary_definition')
                        ->onDelete('cascade');
            $table->date('sallary_date');
            $table->integer('softDelete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sallary');
    }
}
