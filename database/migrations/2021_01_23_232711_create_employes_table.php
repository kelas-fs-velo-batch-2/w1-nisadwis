<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('employee_name');
            $table->date('date_of_birth');
            $table->bigInteger('unit_id');
            $table->foreign('unit_id')
                        ->references('id')
                        ->on('unit')
                        ->onDelete('cascade');
            $table->string('job_title');
            $table->string('employee_status');
            $table->string('profile');
            $table->integer('softDelete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employes');
    }
}
