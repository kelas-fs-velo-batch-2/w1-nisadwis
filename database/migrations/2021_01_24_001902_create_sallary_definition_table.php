<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSallaryDefinitionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sallary_definition', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('basicSallary');
            $table->bigInteger('employes_id');
            $table->foreign('employes_id')
                        ->references('id')
                        ->on('employes')
                        ->onDelete('cascade');
            $table->integer('transport');
            $table->integer('softDelete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sallary_definition');
    }
}
