<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_item', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('product_id');
            $table->foreign('product_id')
                        ->references('id')
                        ->on('product')
                        ->onDelete('cascade');
            $table->bigInteger('sales_id');
            $table->foreign('sales_id')
                        ->references('id')
                        ->on('sales')
                        ->onDelete('cascade');
            $table->integer('qty');
            $table->integer('price');
            $table->integer('softDelete')->default(0);                       
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_item');
    }
}
