<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_item', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('product_id');
            $table->foreign('product_id')
                        ->references('id')
                        ->on('product')
                        ->onDelete('cascade');
            $table->bigInteger('purchase_id');
            $table->foreign('purchase_id')
                        ->references('id')
                        ->on('purchase')
                        ->onDelete('cascade');
            $table->integer('qty');
            $table->integer('price');
            $table->integer('softDelete')->default(0);                       
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_item');
    }
}
