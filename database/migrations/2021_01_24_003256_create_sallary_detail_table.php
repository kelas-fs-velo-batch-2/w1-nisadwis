<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSallaryDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sallary_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sallary_id');
            $table->foreign('sallary_id')
                        ->references('id')
                        ->on('sallary')
                        ->onDelete('cascade');
            $table->integer('basic_sallary');
            $table->integer('sallary_alowance');
            $table->integer('transport');
            $table->integer('presence');
            $table->integer('softDelete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sallary_detail');
    }
}
