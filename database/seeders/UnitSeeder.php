<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('unit')->insert([
            [
                'id' => 1,
                'unit_name' => 'Direksi HRD',
                'parent_id' => 0,
                'sallary_allowance' => '8000000',
               
            ],
            [
                'id' => 2,
                'unit_name' => 'Bagian Human Resource',
                'parent_id' => 1,
                'sallary_allowance' => '7500000',
            
            ],
            [
                'id' => 3,
                'unit_name' => 'Bagian Career Development ',
                'parent_id' => 1,
                'sallary_allowance' => '7000000',
              
            ]
        ]);
    }
}
